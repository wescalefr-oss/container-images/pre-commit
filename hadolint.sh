#!/usr/bin/env bash

export PATH=$PATH:/usr/local/bin

RC=0
for file in $(echo "$@"); do
  echo "Validating ${file}"
  hadolint < ${file}
  RC=$((RC+$?))
done
exit ${RC}