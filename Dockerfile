FROM homebrew/brew AS tfenv
RUN apt-get update && \
  apt-get install -y zip --no-install-recommends && \
  brew install tfenv

FROM alpine:edge
RUN echo "@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
  apk add --no-cache python3 python3-dev py3-pip zip git jq bash curl perl-utils openssh-client gnupg keybase-client@testing build-base shellcheck && \
  pip install pre-commit awscli detect-secrets && \
  ln -s /home/linuxbrew/.linuxbrew/bin/tfenv /bin && \
  ln -s /home/linuxbrew/.linuxbrew/bin/terraform /bin && \
  ln -s /usr/bin/python3 /usr/bin/python && \
  rm -rf /var/cache/apk/*
COPY --from=wata727/tflint /usr/local/bin/tflint /bin
COPY --from=tfenv /home/linuxbrew/.linuxbrew/Cellar/tfenv /home/linuxbrew/.linuxbrew/Cellar/tfenv
COPY --from=tfenv /home/linuxbrew/.linuxbrew/bin /home/linuxbrew/.linuxbrew/bin
COPY --from=docker:dind /usr/local/bin/docker /bin
COPY --from=instrumenta/conftest /conftest /bin
RUN tfenv install latest:^0.12
