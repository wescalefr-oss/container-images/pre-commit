# pre-commit for Container images

Some [pre-commit](https://github.com/pre-commit/pre-commit) hooks to execute on Container images build project:    
- hadolint: runs [hadolint](https://github.com/hadolint/hadolint) to validate every Dockerfile found (`hadolint` command must be available in path)

## General Usage
Add in the file called `.pre-commit-config.yaml` in your repositories with the following declaration:
```
  - repo: https://github.com/gruntwork-io/pre-commit
    rev: <VERSION> # Get the latest from: https://gitlab.com/wescalefr-oss/container-images/pre-commit/-/releases-/releases
    hooks:
      - id: hadolint
```